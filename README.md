# Infrastructure #

This project is responsible for implementing infrastructure required to
support DATAFLOWER services hosted on the cloud.

## Architecture ##

### Objectives ###

1. Attempt to model or build on AWS & Terraform infrastructure best practices.
2. To showcase skills and capabilities.
3. Provide infrastructure to host portfolio projects.
4. To assist in bringing work experience to clients.
5. Provide a foundation and opportunity to develop projects with actual users
  in the future.

### Principles ###

1. Support multiple AWS accounts, especially production and integration
  environments.
2. Different environments are completely isolated from each other.
3. Different teams can potentially have their own AWS account(s) for
  development.
4. Individual developers can standup a subset of the infrastructure and
  connect it to an integration environment.
5. Automate the setting up of the infrastructure for each account.
6. DRY: Identical infrastructure setup using an identical configuration.
  e.g. pre-prod and prod using the same; dev and uat using the same;
7. Do not force DRY idioms when they don't make sense, e.g. production and
  non-production.
8. While non-production and production environments are setup differently,
  we want to maintain a common way developers can reason about them.
9. Centralised development and management of AWS accounts, domain names, IAM
  and VPC, Load Balancer patterns, rather than leaving responsibility for
  these shared elements to individual applications.
10. Keep costs low.

### Implementation & Design Decisions ###

1. There is a Sandbox AWS account and a Production AWS account is planned.
2. Each AWS account has its own VPC and networking stack.
3. Infrastructure including IAM roles are automated via this project.
4. Use of `terraform.workspace` to vary multiple variants of infrastructure
  with identical resources.
  - All resource names prefixed with `${terraform.workspace}`, or for modules,
    `${var.workspace}` which can be passed in as a Terraform variable.
5. Use of directories and modules and shared outputs.tf file to provide a
  common interface to infrastructure with different resources, to allow
  individual applications to build on it without unnecessary duplication.
  Modules allow developers to reuse a configuration in different environments.
  - For example. Multiple development accounts with identical stacks, and
    production accounts with their own stacks, and applications building on
    them using a common interface (i.e. Terraform remote state and outputs)
    for the underlying infrastructure.
6. Separating infrastructure into aspects, e.g. `acm`, `ec2`, `ecs`, `iam`,
  `vpc`, to enable independent deployment and development, and make it easier
  to reason with the infrastructure.
7. All products share the same Sandbox and Production AWS accounts, to reuse
  load balancer, vpc, internet gateways etc. In larger organisations they maybe
  in their own sets of accounts, e.g. to facilitate selling a product.

### Potential improvements ###

1. **As author is not an expert in networking or DevOps**, could use help
  verifying the infrastructure configurations are good, and improve according
  to advice.
2. Domain names, subdomains and how they tie to individual environments and
  workspaces needs more thought.

## Infrastructure & DevOps ##

- [Auth0](https://auth0.com) for developer user management.
- [AWS Console](https://dev-4xo5okqj.au.auth0.com/samlp/jpIFDUsz4xSkOABrbs4nPyAour2lYy03)
  login page.

The following infrastructure have been setup:

- [Auth0 SAML Single Sign-On](https://auth0.com/docs/integrations/aws/sso)
  integration with AWS SAML 2.0.
- [Auth0 Login](https://github.com/binxio/auth0-login) setup for authenticating
  using Auth0 to run [Terraform](http://terraform.io) deployment.
- An S3 bucket in `ap-southeast-2` named `dataflower-terraform-state`.
- A DynamoDB table in `ap-southeast-2` named `terraform-state-lock` with
  `LockID` as the primary key.

### Developer Setup ###

Following the below instructions will enable a developer to deploy the project
on their own machines using [Terraform](http://terraform.io).

1. Run `pipenv install --dev`.
2. Run
```
echo "$(cat << EOF
[DEFAULT]
idp_url=https://dev-4xo5okqj.au.auth0.com
client_id=xizd66t1dbcPRNJVF65VqBTBAL7RxqvT

[DATAFLOWER_Main]
idp_url=https://dev-4xo5okqj.au.auth0.com
client_id=xizd66t1dbcPRNJVF65VqBTBAL7RxqvT
aws_account=dataflower
aws_role=Administrator
aws_profile=DATAFLOWER_Main
EOF
)" > ~/.saml-login
```
3. Run
```
echo "$(cat << EOF
[DEFAULT]
dataflower=915432453250
EOF
)" > ~/.aws-accounts
```
4. Test using `saml-login aws-assume-role --show`.
5. Authenticate using `saml-login -c DATAFLOWER_Main aws-assume-role`.

<strong>Note:</strong>

The [bin/sandbox-aws](bin/sandbox-aws) command available
for the purpose of executing commands in the Sandbox account outside of
Terraform. e.g. for encrypting and decrypting secrets using kms in the Sandbox
account.

### GitLab CI & Terraform Setup ###

This project configures AWS users and roles for deploying
the project from Terminal, and from GitLab, via Terraform.

Permissions are added to the `Deploy` IAM role as they become required for
deployment. i.e. when running a deployment and getting an `AccessDenied`
message. See `aws_iam_policy_attachment`'s attached to
`aws_iam_role.deploy_sandbox` in [src/iam/main.tf](src/iam/main.tf)
for the current set of policies/permissions for the `Deploy` IAM role.

In this directory:

1. Run `AWS_PROFILE=DATAFLOWER_Main terraform init`
2. Run `AWS_PROFILE=DATAFLOWER_Main terraform apply`
3. Export Access Key & Secret to GitLab:
  1. Login to AWS.
  2. Go to the IAM page
  3. Click on `GitLab` user.
  4. [Click `Security Credentials` tab](https://console.aws.amazon.com/iam/home?region=ap-southeast-2#/users/GitLab?section=security_credentials).
  5. Click `Create access key`.
  6. Go to [GitLab CI/CD settings](https://gitlab.com/eric-dataflower/convert-currency/-/settings/ci_cd)
  7. Edit the `Variables` section and add the values for `AWS_ACCESS_KEY_ID`,
    `AWS_SECRET_ACCESS_KEY`.
  8. <strong>Make sure the option for Masked is turned on</strong>.
  9. Add variable `AWS_DEFAULT_REGION`, setting it to `ap-southeast-2`.

This will create the `Deploy` IAM role, add access to the role to GitLab, and
enable the deployment of
[convert-currency](https://gitlab.com/eric-dataflower/convert-currency)
on Terminal and on GitLab.

The above steps are run prior to the first
[convert-currency](https://gitlab.com/eric-dataflower/convert-currency)
deployment and when policies are adjusted for the deployment users and roles
in [main.tf](main.tf).

### Manual Deployment ###

Run `pipenv shell` to gain access to `saml-login` command.

In each of the terraform sub-directories, run the following:

1. Authenticate using `saml-login -c DATAFLOWER_Main aws-assume-role`.
2. Initialize terraform `AWS_PROFILE=DATAFLOWER_Main terraform init`.
3. Preview deployment changes using
  `AWS_PROFILE=DATAFLOWER_Main terraform plan`.
4. Deploy using `AWS_PROFILE=DATAFLOWER_Main terraform apply`.

To destroy the infrastructure:

6. To remove all AWS resources, run
  `AWS_PROFILE=DATAFLOWER_Main terraform destroy`.
