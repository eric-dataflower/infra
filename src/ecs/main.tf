locals {
  # 915432453250 is the main account.
  main_role_arn = "arn:aws:sts::915432453250:role/Administrator"

  # 496027045990 is the sandbox account.
  sandbox_role_arn = "arn:aws:sts::496027045990:role/Administrator"

  # 543318363041 is the crypto account.
  crypto_role_arn = "arn:aws:sts::543318363041:role/Administrator"
}

provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.main_role_arn
  }
}

provider "aws" {
  alias  = "sandbox"
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.sandbox_role_arn
  }
}

provider "aws" {
  alias  = "crypto"
  region = "us-east-1"
  assume_role {
    role_arn = local.crypto_role_arn
  }
}

terraform {
  backend "s3" {
    bucket         = "dataflower-terraform-state"
    key            = "ecs.tfstate"
    region         = "ap-southeast-2"
    encrypt        = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

resource "aws_ecs_cluster" "ecs_cluster_sandbox" {
  provider = aws.sandbox
  name     = "${terraform.workspace}_ecs_cluster"
}

# resource "aws_ecs_cluster" "ecs_cluster_crypto" {
#   provider = aws.crypto
#   name     = "${terraform.workspace}_ecs_cluster"
# }

output "ecs_cluster" {
  value = {
    "sandbox" : aws_ecs_cluster.ecs_cluster_sandbox,
    # "crypto" : aws_ecs_cluster.ecs_cluster_crypto
  }
}
