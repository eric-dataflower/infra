locals {
  # 915432453250 is the main account.
  main_role_arn = "arn:aws:sts::915432453250:role/Administrator"

  # 496027045990 is the sandbox account.
  sandbox_role_arn = "arn:aws:sts::496027045990:role/Administrator"
}

provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.main_role_arn
  }
}

provider "aws" {
  alias  = "sandbox"
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.sandbox_role_arn
  }
}

terraform {
  backend "s3" {
    bucket         = "dataflower-terraform-state"
    key            = "ec2.tfstate"
    region         = "ap-southeast-2"
    encrypt        = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

data "terraform_remote_state" "vpc" {
  backend   = "s3"
  workspace = "default"
  config = {
    region  = "ap-southeast-2"
    bucket  = "dataflower-terraform-state"
    key     = "vpc.tfstate"
    encrypt = "true"
  }
}

data "terraform_remote_state" "acm" {
  backend   = "s3"
  workspace = "default"
  config = {
    region  = "ap-southeast-2"
    bucket  = "dataflower-terraform-state"
    key     = "acm.tfstate"
    encrypt = "true"
  }
}

module "ec2_sandbox" {
  source          = "./modules/dev"
  workspace       = "default"
  certificate_arn = data.terraform_remote_state.acm.outputs.acm_certificates["sandbox"]["*.int.dataflower.com.au"].arn
  vpc             = data.terraform_remote_state.vpc.outputs.vpc["sandbox"]
  providers = {
    aws = aws.sandbox
  }
}

output "ec2" {
  value = {
    "sandbox" : module.ec2_sandbox
  }
}
