provider "aws" {
  region = "ap-southeast-2"
}

locals {
  vpc = var.vpc
}

resource "aws_s3_bucket" "logs" {
  bucket = "${var.workspace}-lb"
  acl    = "private"
  force_destroy = true
}

data "aws_elb_service_account" "lb" {
}

data "aws_iam_policy_document" "lb" {
  statement {
    actions   = ["s3:PutObject"]
    resources = ["arn:aws:s3:::${aws_s3_bucket.logs.bucket}/*"]
    principals {
      type = "AWS"
      identifiers = [data.aws_elb_service_account.lb.arn]
    }
  }
}

resource "aws_s3_bucket_policy" "lb" {
  bucket = aws_s3_bucket.logs.bucket
  policy = data.aws_iam_policy_document.lb.json
}

resource "aws_security_group" "lb" {
  vpc_id = local.vpc.aws_vpc.id
  name = "${var.workspace}-ec2-lb"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "lb" {
  name = "${var.workspace}-lb"
  internal = false
  load_balancer_type = "application"
  security_groups = [aws_security_group.lb.id]
  subnets = local.vpc.public_subnets

  enable_deletion_protection = false

  access_logs {
    bucket  = aws_s3_bucket.logs.bucket
    prefix  = "${var.workspace}-lb"
    enabled = true
  }

  depends_on = [aws_s3_bucket_policy.lb]
}

resource "aws_acm_certificate" "lb" {
  domain_name = "*.${var.domain}"
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "lb" {
  certificate_arn         = aws_acm_certificate.lb.arn

  # As dataflower.com.au is managed externally to AWS,
  # The following domain name must be setup at Cloudflare for the validation.
  validation_record_fqdns = [aws_acm_certificate.lb.domain_validation_options.0.resource_record_name]
}

resource "aws_lb_listener" "https_lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port = "443"
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-2016-08"
  certificate_arn = aws_acm_certificate.lb.arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Not Found"
      status_code  = "404"
    }
  }

  depends_on = [aws_acm_certificate_validation.lb]
}

resource "aws_lb_listener" "http_lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
