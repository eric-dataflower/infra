variable "workspace" {
  type = string
}

variable "domain" {
  type = string
  default = "dataflower.com.au"
}

variable "subdomain" {
  type = string
}

variable "vpc" {
}
