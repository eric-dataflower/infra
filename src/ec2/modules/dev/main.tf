provider "aws" {
  region = "ap-southeast-2"
}

locals {
  vpc = var.vpc
}

resource "aws_s3_bucket" "logs" {
  bucket = "dataflower-${var.workspace}-lb"
  acl    = "private"
  force_destroy = true
}

data "aws_elb_service_account" "lb" {
}

data "aws_caller_identity" "current" {}

# Allow AWS account and Load Balancer account access to the logs bucket.
# Primarily to allow AWS Account to update this bucket via Terraform.
# Could be tightened up to prevent modifications to S3 objects.
data "aws_iam_policy_document" "lb" {
  statement {
    actions   = ["s3:*"]
    resources = ["arn:aws:s3:::${aws_s3_bucket.logs.bucket}/*"]
    principals {
      type = "AWS"
      identifiers = [
        data.aws_elb_service_account.lb.arn,
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      ]
    }
  }
}

resource "aws_s3_bucket_policy" "lb" {
  bucket = aws_s3_bucket.logs.bucket
  policy = data.aws_iam_policy_document.lb.json
}

resource "aws_security_group" "lb" {
  vpc_id = local.vpc.aws_vpc.id
  name = "${var.workspace}-ec2-lb"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "lb" {
  name = "${var.workspace}-lb"
  internal = false
  load_balancer_type = "application"
  security_groups = [aws_security_group.lb.id]
  subnets = local.vpc.public_subnets

  enable_deletion_protection = false

  access_logs {
    bucket  = aws_s3_bucket.logs.bucket
    prefix  = "${var.workspace}-lb"
    enabled = true
  }

  depends_on = [aws_s3_bucket_policy.lb]
}

resource "aws_lb_listener" "lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port = "443"
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-2016-08"
  certificate_arn = var.certificate_arn

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Not Found"
      status_code  = "404"
    }
  }
}

resource "aws_lb_listener" "redirect_lb_listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
