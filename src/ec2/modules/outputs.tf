output "aws_lb" {
  value = aws_lb.lb
}

output "aws_lb_listener" {
  value = aws_lb_listener.lb_listener
}

# The bucket where logs are deposited.
output "aws_s3_bucket" {
  value = aws_s3_bucket.logs
}
