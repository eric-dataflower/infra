# EC2 #

1. Setup the load balancer using `terraform apply`
2. Manually connect the DNS of the domain name, e.g. `mydomain.dataflower.com.au`, to the corresponding load balancer DNS name, e.g. `default-lb-708881972.ap-southeast-2.elb.amazonaws.com`.
