locals {
  # 915432453250 is the main account.
  main_role_arn = "arn:aws:sts::915432453250:role/Administrator"

  # 496027045990 is the sandbox account.
  sandbox_role_arn = "arn:aws:sts::496027045990:role/Administrator"
}

provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.main_role_arn
  }
}

provider "aws" {
  alias = "sandbox"
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.sandbox_role_arn
  }
}

terraform {
  backend "s3" {
    bucket = "dataflower-terraform-state"
    key = "acm.tfstate"
    region = "ap-southeast-2"
    encrypt = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

resource "aws_acm_certificate" "sandbox_acm_certificates" {
  provider = aws.sandbox
  for_each = toset([
    "*.int.dataflower.com.au"
  ])
  domain_name = each.value
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

output "acm_certificates" {
  value = {
    "sandbox": aws_acm_certificate.sandbox_acm_certificates
  }
}
