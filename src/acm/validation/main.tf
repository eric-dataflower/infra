locals {
  # 915432453250 is the main account.
  main_role_arn = "arn:aws:sts::915432453250:role/Administrator"

  # 496027045990 is the sandbox account.
  sandbox_role_arn = "arn:aws:sts::496027045990:role/Administrator"
}

provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.main_role_arn
  }
}

provider "aws" {
  alias = "sandbox"
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.sandbox_role_arn
  }
}

terraform {
  backend "s3" {
    bucket = "dataflower-terraform-state"
    key = "acm_validation.tfstate"
    region = "ap-southeast-2"
    encrypt = "true"
    dynamodb_table = "terraform-state-lock"
  }
}

data "terraform_remote_state" "acm" {
  backend = "s3"
  workspace = "default"
  config = {
    region = "ap-southeast-2"
    bucket = "dataflower-terraform-state"
    key = "acm.tfstate"
    encrypt = "true"
  }
}

resource "aws_acm_certificate_validation" "sandbox" {
  provider = aws.sandbox
  for_each = data.terraform_remote_state.acm.outputs.acm["sandbox"]
  certificate_arn = each.value.arn
  validation_record_fqdns = [each.value.domain_validation_options.0.resource_record_name]
}
