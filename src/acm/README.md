# ACM #

1. Create the certificates by running `terraform apply`.
2. Configure `domain_validation_options`, at the DNS provider, i.e.:
  ```
  "domain_validation_options" = [
    {
      "domain_name" = "*.int.dataflower.com.au"
      "resource_record_name" = "_4b74ba867403487f6674a99c056d732f.int.dataflower.com.au."
      "resource_record_type" = "CNAME"
      "resource_record_value" = "_b614340f4a7971c91f48391a00ef36bf.nhqijqilxf.acm-validations.aws."
    },
  ]
  ```
3. `cd validation` and run `terraform apply` to validate the certificates.
