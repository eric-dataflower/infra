locals {
  # 915432453250 is the main account.
  #   main_role_arn = "arn:aws:sts::915432453250:role/Administrator"

  # 496027045990 is the sandbox account.
  sandbox_role_arn = "arn:aws:sts::496027045990:role/Administrator"

  # 543318363041 is the crypto account.
  crypto_role_arn = "arn:aws:sts::543318363041:role/Administrator"
}

# provider "aws" {
#   region = "ap-southeast-2"
#   assume_role {
#     role_arn = local.main_role_arn
#   }
# }

provider "aws" {
  alias  = "sandbox"
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.sandbox_role_arn
  }
}

provider "aws" {
  alias  = "crypto"
  region = "ap-southeast-1"
  assume_role {
    role_arn = local.crypto_role_arn
  }
}

terraform {
  backend "s3" {
    bucket         = "dataflower-terraform-state"
    key            = "vpc.tfstate"
    region         = "ap-southeast-2"
    encrypt        = "true"
    dynamodb_table = "terraform-state-lock"
  }
}
module "vpc_crypto" {
  source = "./modules/dev"
  providers = {
    aws = aws.crypto
  }
  public_subnets_az_cidr_blocks = {
    "ap-southeast-1a" : "10.0.0.0/21",
    "ap-southeast-1b" : "10.0.8.0/21",
  }
  region = "ap-southeast-1"
}

module "vpc_sandbox" {
  source = "./modules/dev"
  providers = {
    aws = aws.sandbox
  }
  region = "ap-southeast-2"
}

output "vpc" {
  value = {
    "sandbox" : module.vpc_sandbox,
    "crypto" : module.vpc_crypto
  }
}
