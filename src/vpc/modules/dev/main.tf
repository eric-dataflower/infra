# A template for a development account network configuration.
# TODO: Add private subnets, parameterize public_subnets_az_cidr_blocks.

provider "aws" {
  region = var.region
}


locals {
  public_subnets_az_cidr_blocks = var.public_subnets_az_cidr_blocks
  public_subnets = [
    for aws_subnet in aws_subnet.public_subnets :
    aws_subnet.id
  ]
}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_route_table" "internet_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }
}

resource "aws_route_table_association" "public_subnets" {
  for_each       = local.public_subnets_az_cidr_blocks
  subnet_id      = aws_subnet.public_subnets[each.key].id
  route_table_id = aws_route_table.internet_route_table.id
}

resource "aws_subnet" "public_subnets" {
  for_each                = local.public_subnets_az_cidr_blocks
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = each.value
  map_public_ip_on_launch = true
  availability_zone       = each.key
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id
}
