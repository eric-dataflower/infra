variable "public_subnets_az_cidr_blocks" {
  type = map(string)
  default = {
    "ap-southeast-2a" : "10.0.0.0/21",
    "ap-southeast-2b" : "10.0.8.0/21",
  }
}

variable "region" {
  type    = string
  default = "ap-southeast-2"
}
