# A template for a production account network configuration.
# TODO: Set up network.

provider "aws" {
  region = "ap-southeast-2"
}

locals {
  public_subnets = []
}

resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.vpc.id
}
