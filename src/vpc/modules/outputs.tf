# Define common outputs for all network configurations.

output "aws_vpc" {
  value = aws_vpc.vpc
}

output "aws_internet_gateway" {
  value = aws_internet_gateway.internet_gateway
}

output "public_subnets" {
  value = local.public_subnets
}
