resource "aws_iam_role" "iam_role" {
  name               = "Deploy"
  assume_role_policy = var.assume_role_policy
}

resource "aws_iam_role_policy" "iam_role" {
  role   = aws_iam_role.iam_role.id
  policy = data.aws_iam_policy_document.iam_role.json
}

data "aws_caller_identity" "current" {}

data "aws_iam_policy_document" "iam_role" {
  statement {
    actions = [
      "ec2:AuthorizeSecurityGroupEgress",
      "ec2:AuthorizeSecurityGroupIngress",
      "ec2:DeleteSecurityGroup",
      "ec2:RevokeSecurityGroupEgress",
      "ec2:RevokeSecurityGroupIngress"
    ]
    resources = [
      "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:security-group/*"
    ]
  }

  statement {
    actions = [
      "ec2:DescribeNetworkInterfaces"
    ]
    resources = [
      "*"
    ]
  }

  statement {
    actions = [
      "ec2:DeleteInternetGateway"
    ]
    resources = [
      "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:internet-gateway/*"
    ]
  }

  statement {
    actions = [
      "s3:PutBucketPolicy",
      "s3:GetBucketPolicy",
    ]
    resources = [
      "arn:aws:s3:::*"
    ]
  }

  statement {
    actions = [
      "ec2:DeleteRouteTable"
    ]
    resources = [
      "arn:aws:ec2:*:${data.aws_caller_identity.current.account_id}:route-table/*"
    ]
  }

  statement {
    actions = [
      "kms:*"
    ]
    resources = [
      "arn:aws:kms:ap-southeast-2:${data.aws_caller_identity.current.account_id}:key/*",
      "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:key/*"
    ]
  }
  statement {
    actions = [
      "kms:CreateKey"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    actions = [
      "ssm:PutParameter",
      "ssm:GetParameter",
      "ssm:GetParameters",
      "ssm:GetParametersByPath",
      "ssm:PutParameter",
      "ssm:DescribeParameters",
      "ssm:ListTagsForResource",
      "ssm:DeleteParameter"
    ]
    resources = [
      "arn:aws:ssm:*:${data.aws_caller_identity.current.account_id}:*"
    ]
  }
}

# Managed Policies

resource "aws_iam_role_policy_attachment" "ElasticLoadBalancingFullAccess" {
  role       = aws_iam_role.iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/ElasticLoadBalancingFullAccess"
}

resource "aws_iam_role_policy_attachment" "IAMFullAccess" {
  role = aws_iam_role.iam_role.name

  policy_arn = "arn:aws:iam::aws:policy/IAMFullAccess"
}

resource "aws_iam_role_policy_attachment" "AmazonECS_FullAccess" {
  role       = aws_iam_role.iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}

resource "aws_iam_role_policy_attachment" "AWSAppSyncAdministrator" {
  role       = aws_iam_role.iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSAppSyncAdministrator"
}

resource "aws_iam_role_policy_attachment" "SecretsManagerReadWrite" {
  role       = aws_iam_role.iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/SecretsManagerReadWrite"
}

resource "aws_iam_role_policy_attachment" "AWSStepFunctionsFullAccess" {
  role       = aws_iam_role.iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSStepFunctionsFullAccess"
}
