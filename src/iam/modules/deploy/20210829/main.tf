
module "base" {
  source = "../base"
  assume_role_policy = var.assume_role_policy
}

resource "aws_iam_role_policy_attachment" "AWSLambda_FullAccess" {
  role       = module.base.aws_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambda_FullAccess"
}
