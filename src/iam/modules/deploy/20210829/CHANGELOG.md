# 20210829

- Renamed AWSLambdaFullAccess to AWSLambda_FullAccess
  > https://docs.aws.amazon.com/lambda/latest/dg/access-control-identity-based.html
  >
  > Note
  > The AWS managed policies AWSLambdaFullAccess and AWSLambdaReadOnlyAccess will
  > be deprecated on March 1, 2021. After this date, you cannot attach these
  > policies to new IAM users. For more information, see the related
  > troubleshooting topic.
