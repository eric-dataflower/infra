
module "base" {
  source = "../base"
  assume_role_policy = var.assume_role_policy
}

resource "aws_iam_role_policy_attachment" "AWSLambdaFullAccess" {
  role       = module.base.aws_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/AWSLambdaFullAccess"
}
