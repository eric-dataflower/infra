locals {
  # 915432453250 is the main account.
  main_role_arn = "arn:aws:sts::915432453250:role/Administrator"

  # 496027045990 is the sandbox account.
  sandbox_role_arn = "arn:aws:sts::496027045990:role/Administrator"

  # 543318363041 is the crypto account.
  crypto_role_arn = "arn:aws:sts::543318363041:role/Administrator"
}

provider "aws" {
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.main_role_arn
  }
}

provider "aws" {
  alias  = "sandbox"
  region = "ap-southeast-2"
  assume_role {
    role_arn = local.sandbox_role_arn
  }
}

provider "aws" {
  alias  = "crypto"
  region = "ap-southeast-1"
  assume_role {
    role_arn = local.crypto_role_arn
  }
}

terraform {
  backend "s3" {
    bucket         = "dataflower-terraform-state"
    key            = "iam.tfstate"
    region         = "ap-southeast-2"
    encrypt        = "true"
    dynamodb_table = "terraform-state-lock"
  }
}


data "terraform_remote_state" "vpc" {
  backend   = "s3"
  workspace = "default"
  config = {
    region  = "ap-southeast-2"
    bucket  = "dataflower-terraform-state"
    key     = "vpc.tfstate"
    encrypt = "true"
  }
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::915432453250:root",
        aws_iam_user.gitlab.arn,
        local.main_role_arn
      ]
    }
  }
}

module "deploy_sandbox" {
  source = "./modules/deploy/20210829"
  providers = {
    aws = aws.sandbox
  }
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
  #   vpc                = data.terraform_remote_state.vpc.outputs.vpc["sandbox"].aws_vpc
}

module "deploy_crypto" {
  source = "./modules/deploy/20210829"
  providers = {
    aws = aws.crypto
  }
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_user" "gitlab" {
  name = "Gitlab"
}

resource "aws_iam_user_policy" "gitlab" {
  name   = "gitlab-policy"
  user   = aws_iam_user.gitlab.name
  policy = data.aws_iam_policy_document.gitlab.json
}

# Allow Gitlab to use Terraform and update remote state.
data "aws_iam_policy_document" "gitlab" {
  statement {
    actions = ["sts:AssumeRole"]
    resources = [
      module.deploy_sandbox.aws_iam_role.arn,
      module.deploy_crypto.aws_iam_role.arn
    ]
  }
  statement {
    actions = [
      "dynamodb:GetItem",
      "dynamodb:PutItem",
      "dynamodb:DeleteItem"
    ]
    resources = [
      "arn:aws:dynamodb:ap-southeast-2:915432453250:table/terraform-state-lock"
    ]
  }
  statement {
    actions = [
      "s3:ListBucket"
    ]
    resources = [
      "arn:aws:s3:::dataflower-terraform-state",
      "arn:aws:s3:::dataflower-terraform-state/*"
    ]
  }
  statement {
    actions = [
      "s3:GetObject",
      "s3:PutObject"
    ]
    resources = ["arn:aws:s3:::dataflower-terraform-state/*"]
  }
}
